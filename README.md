# wpf_EntityFramework_WT01 README #

This is the included solution with the article 'WPF: EntityFramework MVVM Walk Through 1 Sample' [here](https://gallery.technet.microsoft.com/WPF-EntityFramework-MVVM-c7167301). The solution used .net framework 4.5.1.  I'll be attempting the following:

* use .Net framework 4.6.2 - Success
* In Nuget, consolidated several .Net versions to 5.6.4 - Success
* make a WCF service to the SalesSystem dB on an MS SQL Server 2014 - Success
* use RESTful services to connect to the WCF service - Fail

I give up. I thought it might have been trivial to convert the SalesContext references to use the SalesSystemService (WCF) that I implemented, but somehow they are not on a 1-to-1 par with each other.  This walkthrough has specific Linq commands that makes the application be aware of how the data is obtained.  REST on the other hand does not use these Linq commands.  Therefore, to properly use REST, I would need to implement a whole new database lookup code for this application.  If I'm going that route, I'm better off making a new application from scratch to use RESTful services.

I think.

Sryn